import React from 'react';
import { RootStack } from './src/navigation/index'
import { AuthProvider } from './src/provider/authcontext';

const App = () => {
  return (
    <AuthProvider>
      <RootStack />
    </AuthProvider>
  );
};
export default App;

