import React, { useState } from 'react'
import { Alert, SafeAreaView, StyleSheet, View } from 'react-native'
import { Button, Input } from '../components'
import { useCollection, useDocument } from '../hook/useCollection'

export const Report: React.FC<any> = ({ navigation, route }) => {
  const { author } = route.params
  const { createDocument } = useCollection(`users/${author}/report`,)
  const [messege, setMessege] = useState('')

  const Report = async () => {
    await createDocument({ messege: messege });
    await navigation.navigate('Home')
    await Alert.alert('reported')
  }

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Input type='default' state={messege} setstate={setMessege} ifObject={'location'} placeholder='Report' InputType={'notePad'} isObject={false} />
        <Button text='Post' onClick={() => { Report() }} type='Primary' />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-evenly',
    height: '80%',
    alignItems: 'center'
  }
})