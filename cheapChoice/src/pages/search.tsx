import React, { useEffect, useState } from 'react';
import { FlatList, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { ProductCard } from '../components';
import { useCollection } from '../hook/useCollection';
import { SearchIcon } from '../icons';

export const Search = () => {
    const { collection: fullData, isLoading: loading } = useCollection('post')
    const [filteredData, setFilteredData] = useState([])
    const [masterData, setMasterData] = useState([])
    const [search, setSearch] = useState('')
    
    useEffect(() => {
        setFilteredData(fullData);
        setMasterData(fullData);
    }, [fullData])

    const Search = (text: string) => {
        if (text) {
            const newData = masterData.filter((item: any) => {
                const itemData: any = item.name ? item.name.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase()
                return itemData.indexOf(textData) > -1
            }).sort((a: any, b: any) => {
                return a.value - b.value;
            });
            setFilteredData(newData);
        } else {
            setFilteredData(masterData)
        }
        setSearch(text)
    }

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.inputContainer}>
                        <SearchIcon />
                        <TextInput
                            style={{ width: '70%' }}
                            placeholder={'search'}
                            value={search}
                            onChangeText={(text: string) => Search(text)}
                        />
                    </View>
                </View>
                <FlatList
                    data={filteredData}
                    renderItem={ProductCard}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
    inputContainer: {
        borderRadius: 15,
        height: 48,
        width: '90%',
        borderWidth: 2,
        borderStyle: 'solid',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
    },
})