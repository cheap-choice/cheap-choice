import React, { useContext, useState } from 'react'
import { Alert, SafeAreaView, StyleSheet, View } from 'react-native'
import { Button, Input } from '../components'
import { useCollection, useDocument } from '../hook/useCollection'
import { AuthContext } from '../provider/authcontext'

export const Post: React.FC<any> = ({ navigation }) => {
  const { createDocument } = useCollection('post')
  const { cred } = useContext(AuthContext)
  const [postData, setPostData] = useState<any>({
    value: null,
    name: null,
    location: null,
    author: cred.uid,
  })

  console.log(postData ,cred.uid)

  const PostUpload = async () => {
    await createDocument(postData);
    await navigation.navigate('Home')
    await Alert.alert('post uploaded')
  }

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Input type='numeric' state={postData} ifObject={'value'} placeholder='unee bichnuuu' setstate={setPostData} InputType={'text'} isObject={true} />
        <Input type='default' state={postData} ifObject={'name'} placeholder='baraanii ner' setstate={setPostData} InputType={'text'} isObject={true} />
        <Input type='default' state={postData} ifObject={'location'} placeholder='bairshil' setstate={setPostData} InputType={'notePad'} isObject={true} />
        <Button text='Post' onClick={() => { PostUpload() }} type='Primary' />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-evenly',
    height: '80%',
    alignItems: 'center'
  }
})