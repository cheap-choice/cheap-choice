import React, { useContext, useEffect } from 'react'
import { SafeAreaView, StyleSheet, View } from 'react-native'
import { Button } from '../components/button'
import { useDocument } from '../hook/useCollection'
import { UserIcon } from '../icons'
import { AuthContext } from '../provider/authcontext'

export const Welcome: React.FC<any> = ({ navigation }: any) => {
    const { cred } = useContext(AuthContext)
    const { doc } = useDocument(`users/${cred?.uid}`)

    if (doc) {
        console.log(doc)
        navigation.navigate('Home')
    }

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <UserIcon />
                <Button type='Primary' text='Login' onClick={() => navigation.navigate('Login')} />
                <Button type='Secondary' text='Signup' onClick={() => navigation.navigate('Signup')} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        height: 500,
        marginTop: 48,
    },

})