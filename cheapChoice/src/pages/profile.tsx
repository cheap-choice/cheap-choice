import React, { useContext, useEffect, useState } from 'react'
import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import { useDocument } from '../hook/useCollection'
import { UserIcon } from '../icons'
import { AuthContext } from '../provider/authcontext'

export const Profile: React.FC<any> = () => {
    const { cred } = useContext(AuthContext)
    const { doc } = useDocument(`users/${cred?.uid}`)

    return (
        <SafeAreaView>
            <View style={styles.container}>
                {doc &&
                    <>
                        <UserIcon />
                        <Text>
                            firstname: {doc?.firstName}
                        </Text>
                        <Text>
                            lastname: {doc?.lastName}
                        </Text>
                        <Text>
                            number: {doc?.number}
                        </Text>
                    </>
                }
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 500,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }

})