import React, { useContext } from 'react'
import { FlatList, SafeAreaView, ScrollView } from 'react-native'
import { Messege } from '../components/messege'
import { useCollection } from '../hook/useCollection'
import { AuthContext } from '../provider/authcontext'

export const Notification = () => {

    const { cred } = useContext(AuthContext)
    const { collection: data, isLoading: loading } = useCollection(`users/${cred.uid}/report`)
    
    return (
        <SafeAreaView>
            <ScrollView>
                <FlatList
                    data={data}
                    renderItem={Messege}
                    keyExtractor={item => item.id}
                />
            </ScrollView>
        </SafeAreaView>
    )
}