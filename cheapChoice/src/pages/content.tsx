import React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import { Button, ContentText } from '../components';

export const ContentPage: React.FC<any> = ({ navigation, route }) => {
  const { author, location, name, value } = route.params.item

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View>
          <ContentText title={'une'} text={value} />
          <ContentText title={'baraa'} text={name} />
          <ContentText title={'bairshil'} text={location} />
        </View>
        <View style={styles.center}>
          <Button text={'report'} type={'Primary'} onClick={() => navigation.navigate('REPORT', { author })} />
        </View>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 25,
    color: 'black'
  },
  container: {
    width: '100%',
    height: '80%',
    display: 'flex',
    justifyContent:'space-between'
  },
  center: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})