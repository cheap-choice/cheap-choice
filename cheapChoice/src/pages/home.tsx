import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import { ProductCard } from '../components/card'
import { useCollection } from '../hook/useCollection'

export const Home: React.FC<any> = () => {
    const { collection: data, isLoading: loading } = useCollection('post')

    return (
        <SafeAreaView>
            <ScrollView>
                <FlatList
                    data={data}
                    renderItem={ProductCard}
                    keyExtractor={(item) => item.id}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    FlatList: {
        display: 'flex',
        justifyContent: 'center'
    }

})