import React, { useContext, useState } from 'react'
import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import { Button, Input } from '../components/index'
import { useDocument } from '../hook/useCollection'
import { AuthContext } from '../provider/authcontext'

export const Auth: React.FC<any> = ({ navigation }) => {
  const { sendAuthCode, verifyAuthCode, cred, error, setError } = useContext(AuthContext);
  const { doc, setDocument } = useDocument(`users/${cred?.uid}`)
  const [step, setStep] = useState<number>(1);
  const [code, setcode] = useState('');
  const [user, setuser] = useState({
    number: null,
    firstName: null,
    lastName: null,
  });

  console.log(doc, 'doc', cred?.uid)

  const StepStep = async () => {
    setError(null)
    switch (step) {
      case 1:
        await sendAuthCode(user.number);
        await setStep(2)
        break;
      case 2:
        await verifyAuthCode(code);
        await doc ? navigation.navigate('Home') : setStep(3);
        break;
      case 3:
        if (cred.uid) {
          await setDocument(user);
        }
        await navigation.navigate('Home');
        break;
    }
  }

  return (
    <SafeAreaView>
      <View style={[styles.container]}>
        {step == 1 &&
          <Input isObject={true} type='numeric' state={user} setstate={setuser} ifObject={'number'} placeholder='phone number' InputType={'text'} />
        }
        {step == 2 &&
          <Input isObject={false} type='numeric' state={code} setstate={setcode} ifObject={'code'} placeholder='verify code' InputType={'text'} />
        }
        {step == 3 &&
          <>
            <Input isObject={true} state={user} setstate={setuser} ifObject={'firstName'} placeholder='firstname' type='default' InputType='text' />
            <Input isObject={true} state={user} setstate={setuser} ifObject={'lastName'} placeholder='lastname' type='default' InputType='text' />
          </>
        }
      </View>
      <View style={styles.textContainer}>
        <Text style={{ fontSize: 14, color: 'red' }}>
          {error &&
            'something is wrong'
          }
        </Text>
      </View>
      <View style={[styles.Button]}>
        <Button type='Primary' text={step == 1 ? 'send code' : step == 2 ? 'verify code' : 'authorize'} onClick={() => StepStep()} />
      </View>
    </SafeAreaView >
  )
}
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: 200,
    marginTop: 48,
  },
  textContainer: {
    marginTop: 150,
    width: '100%',
    padding: 8,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Button: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 60,
    marginTop: 50
  }
})
