import { types } from '@babel/core'
import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'



type ButtonType = {
  type: 'Primary' | 'Secondary' | 'Subtle',
  text: string,
  onClick: Function | null,
}
export const Button: React.FC<ButtonType> = ({ type, text, onClick }) => {
  return (
    <TouchableOpacity onPress={() => onClick && onClick()}>
      <View style={[styles.button, styles.[`${type}`]]}>
        <Text style={styles.[`Font${type}`]}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  button: {
    width: 200,
    height: 60,
    borderRadius: 40,
    fontSize: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Primary: {
    backgroundColor: '#5F2EEA',
  },
  Secondary: {
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#5F2EEA',
  },
  Subtle: {
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#D9DBE9'!,
  },
  FontPrimary: {
    color: '#FFFFFF'
  },
  FontSecondary: {
    color: '#5F2EEA',
  },
  FontSubtle: {
    color: '#5F2EEA',
  }
})