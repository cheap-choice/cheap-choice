import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export const Messege: React.FC<any> = ({ item }) => {
  return (
    <View style={styles.center}>
      <View style={styles.NotePad}>
        <Text >
          {item.messege}
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  NotePad: {
    maxWidth: 325,
    minHeight: 128,
    width: '90%',
    borderWidth: 2,
    borderStyle: 'solid',
    backgroundColor: '#EFF0F7',
    paddingHorizontal: 23,
    paddingVertical: 20,
    borderRadius: 16,
    fontSize: 20,
    color: 'black',
  },
  center: {
    width: '100%',
    padding: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
})