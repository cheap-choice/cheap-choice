import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


type text = {
  title: string,
  text: string,
}

export const ContentText: React.FC<text> = ({ title, text }) => {
  return (
    <View style={styles.container}>
      <Text style={{fontSize:25}}>
        {`${title}:${text}`}  
      </Text>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 8,
  }
})