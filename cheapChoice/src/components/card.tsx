import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const navigation = useNavigation();

export const ProductCard: React.FC<any> = ({ item }) => {

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation?.navigate('Content', { item })}
      >
        <View style={styles.card}>
          {/* <View style={styles.image}>
          </View> */}
          <View style={styles.title}>
            <Text style={{ fontSize: 25}} >
              baraa: {item.name}
            </Text>
            <Text style={{ fontSize: 20 }} >
              une:{item.value}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    width: 293,
    maxWidth: '90%',
    height: 110,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: 'black',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 20,
    display: 'flex',
    flexDirection: 'row',
    padding:10,
    alignItems: 'center'
  },
  title: {
    height: 100,
    width: '100%',
    fontSize: 25,
    color: 'black',
    padding: 5,
    display: 'flex',
    justifyContent: 'space-between'
  },
  container: {
    width: '100%',
    height: 150,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    color: 'white',
    backgroundColor: 'grey'
  }
})