import React, { useRef, useState } from 'react'
import { StyleSheet, TextInput, View } from 'react-native'

type InputProps = {
  type: 'numeric' | 'default',
  state: any,
  ifObject: string,
  setstate: Function,
  placeholder: string,
  InputType: 'text' | 'notePad',
  isObject: boolean,
}


export const Input: React.FC<InputProps> = ({ isObject, ifObject, state, setstate, placeholder, InputType, type }) => {
  const [active, setactive] = useState<Boolean>(false)
  const [value, setvalue] = useState<string>('')
  const NoteRef = useRef<any>()

  return (
    <>
      {
        InputType == 'text' ?
          <View style={[styles.box, typeof state == 'string' ? state && styles.active : state?.[`${ifObject}`] && styles.active]}>
            <TextInput
              style={styles.input}
              placeholder={placeholder}
              onChangeText={(text: string) => isObject == false ? setstate(text) : setstate({ ...state, [`${ifObject}`]: text })}
              value={typeof state == 'string' ? state : state?.[`${ifObject}`]}
              keyboardType={type ? type : 'default'}
            />
          </View>
          :
          <View style={[styles.NotePad, typeof state == 'string' ? state && styles.active : state?.[`${ifObject}`] && styles.active]}>
            <TextInput
              style={[styles.NoteInput, { marginBottom: 10 },]}
              placeholder={placeholder}
              onChangeText={(text: string) => isObject == false ? setstate(text) : setstate({ ...state, [`${ifObject}`]: text })}
              value={typeof state == 'string' ? state : state?.[`${ifObject}`]}
              keyboardType={type ? type : 'default'}
              multiline={true}
              ref={NoteRef}
            />
          </View>
      }
    </>
  )

}
const styles = StyleSheet.create({
  NotePad: {
    maxWidth: 325,
    minHeight: 128,
    width: '90%',
    backgroundColor: '#EFF0F7',
    paddingHorizontal: 23,
    paddingVertical: 20,
    borderRadius: 16,
    borderWidth: 1,
  },
  NoteInput: {
    width: '100%',
  },
  box: {
    width: 325,
    height: 64,
    backgroundColor: '#EFF0F7',
    borderRadius: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,

  },
  active: {
    borderColor: '#14142B',
    borderStyle: 'solid',
    borderWidth: 3,
  },
  input: {
    width: 276,
    height: 28,
  },
  title: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  }
})