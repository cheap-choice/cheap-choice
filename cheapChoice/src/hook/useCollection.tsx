import firestore from '@react-native-firebase/firestore';
import { useEffect, useState } from 'react';

export const useDocument = (path: string) => {
  const [doc, setDoc] = useState<any>();

  useEffect(() => {
    firestore().doc(path).get().then((data: any) => setDoc(data.data()))
  }, [path])

  const setDocument = (data: object) => {
    firestore().doc(path).set({ ...data, }, { merge: true });
  };

  return { doc, setDocument }
};

export const useCollection = (path: any) => {
  const [collection, setcollection] = useState<any>()
  const [isLoading, setIsLoading] = useState<boolean>(true)

  const createDocument = async (data: object) => {
    firestore().collection(path).add({ ...data, })
  };

  useEffect(() => {
    setIsLoading(true)
    if (path) {
      firestore()
        .collection(path)
        .onSnapshot(async (querySnapshot) => {
          await setcollection(querySnapshot.docs.map((doc) => doc.data()));
          await setIsLoading(false)
        })
    };
  }, [path]);

  return { collection, isLoading, createDocument }
};
