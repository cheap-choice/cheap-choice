import auth from '@react-native-firebase/auth';
import React, { createContext, useEffect, useState } from 'react';
import { useDocument } from '../hook/useCollection';

export const AuthProvider: React.FC<any> = ({ children }: any) => {
  // const {} = useDocument();
  const [confirmation, setConfirmation] = useState<any>(null);
  const [cred, setCred] = useState();
  const [error, setError] = useState()

  useEffect(() => {
    const subscribe = auth().onAuthStateChanged(async (user: any) => {
      if (user) {
        setCred(user)
      }
    });
    return () => subscribe()
  }, [])

  const sendAuthCode = async (Number: any) => {
    try {
      const confirmationResult = await auth().signInWithPhoneNumber(`+976${Number}`);
      setConfirmation(confirmationResult);
    } catch (error) {
      setError(error.toString())
      throw error;
    }
  };

  const verifyAuthCode = async (confirmationCode: any) => {
    try {
      await confirmation.confirm(confirmationCode)
    } catch (error) {
      setError(error.toString())
      throw error
    }
  };

  const signOut = async () => {
    await auth().signOut();
  };


  return (
    <AuthContext.Provider
      value={{
        error,
        cred,
        verifyAuthCode,
        sendAuthCode,
        signOut,
        setError,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const AuthContext = createContext<{
  error: any;
  cred: any;
  setError: (errorValue: any) => void;
  sendAuthCode: (Number: any) => void;
  verifyAuthCode: (confirmationCode: any) => void;
  signOut: () => void;
}>({
  error: null,
  cred: null,
  setError: Function,
  verifyAuthCode: Function,
  sendAuthCode: Function,
  signOut: Function,
});