import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { View } from 'react-native'

export const UserIcon = () => {
  return (
    <Svg width="128" height="128" viewBox="0 0 128 128" fill="none">
      <Path d="M16 114.667L18.3321 99.6743C18.5407 98.3335 19.4082 97.1946 20.6695 96.694C26.5111 94.3759 44.3629 88 64 88C83.6371 88 101.489 94.3759 107.33 96.694C108.592 97.1946 109.459 98.3335 109.668 99.6743L112 114.667" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      <Path d="M64.0007 64.0001C78.7282 64.0001 90.6673 52.061 90.6673 37.3334C90.6673 22.6058 78.7282 10.6667 64.0007 10.6667C49.2731 10.6667 37.334 22.6058 37.334 37.3334C37.334 52.061 49.2731 64.0001 64.0007 64.0001Z" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </Svg>
  )
}