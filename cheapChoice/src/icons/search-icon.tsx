import React from 'react';
import Svg, { Path } from 'react-native-svg';

export const SearchIcon = () => {
  return (
      <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
        <Path d="M11 20C15.9706 20 20 15.9706 20 11C20 6.02944 15.9706 2 11 2C6.02944 2 2 6.02944 2 11C2 15.9706 6.02944 20 11 20Z" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <Path d="M22 22L18 18" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </Svg>
  )
}