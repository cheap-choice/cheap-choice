import React from 'react';
import Svg, { Rect } from 'react-native-svg';

export const PostButtonIcon = () => {
  return (
    <Svg width="28" height="29" viewBox="0 0 28 29" fill="none">
      <Rect x="0.0351562" y="13.2544" width="27.9273" height="2.32727" rx="1.16364" fill="black" />
      <Rect x="12.8359" y="0.45459" width="2.32727" height="27.9273" rx="1.16364" fill="black" />
    </Svg>
  )
}