import React from 'react';
import Svg, { Path } from 'react-native-svg';

export const CloseIcon = () => {
  return (
    <Svg width="24" height="24" viewBox="0 0 24 24" fill="none">
      <Path d="M6 6L18.7742 18.7742" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      <Path d="M6 18.7742L18.7742 5.99998" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </Svg>
  )
}