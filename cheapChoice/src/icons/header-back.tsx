import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { View } from 'react-native';

export const HeaderBack = () => {
    return (
        <View style={{ margin: 16 }}>
            <Svg width="12" height="22" viewBox="0 0 12 22" fill="none" >
                <Path d="M11 1L1 11L11 21" stroke="#14142B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            </Svg>
        </View>
    )
}