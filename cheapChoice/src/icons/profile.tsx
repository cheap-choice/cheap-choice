import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { View } from 'react-native'

export const ProfileIcon = () => {
  return (
    <View style={{ margin: 16 }}>
      <Svg width="28" height="28" viewBox="0 0 28 28" fill="none" >
        <Path d="M3.5 25.0833L3.74881 23.4839C3.95739 22.143 4.83257 20.996 6.12418 20.5799C7.97554 19.9834 10.8851 19.25 14 19.25C17.1149 19.25 20.0245 19.9834 21.8758 20.5799C23.1674 20.996 24.0426 22.143 24.2512 23.4839L24.5 25.0833" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
        <Path d="M13.9993 14.0002C17.221 14.0002 19.8327 11.3885 19.8327 8.16683C19.8327 4.94517 17.221 2.3335 13.9993 2.3335C10.7777 2.3335 8.16602 4.94517 8.16602 8.16683C8.16602 11.3885 10.7777 14.0002 13.9993 14.0002Z" stroke="#111111" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      </Svg>
    </View>
  )
}