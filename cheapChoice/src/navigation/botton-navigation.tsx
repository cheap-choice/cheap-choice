import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Home, Notification, Search } from '../pages/index';
import { BottomTab } from './bottom-menu';
const Navigation = createBottomTabNavigator()


export const BottomTabNavigation = () => {
  return (
      <Navigation.Navigator tabBar={(props) => <BottomTab {...props} />}>
        <Navigation.Screen name={'Home'} component={Home} />
        <Navigation.Screen name={'Search'} component={Search} />
        <Navigation.Screen name={'Notification'} component={Notification} />
      </Navigation.Navigator>
  )
}