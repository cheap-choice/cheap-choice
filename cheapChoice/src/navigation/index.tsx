import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useContext } from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { HeaderBack, ProfileIcon, SignOutIcon } from '../icons/index';
import { Auth, ContentPage, Notification, Post, Profile, Report, Search, Welcome } from '../pages/index';
import { AuthContext } from '../provider/authcontext';
import { BottomTabNavigation } from './botton-navigation';

const Roots = createStackNavigator()


export const RootStack = () => {
    const { signOut } = useContext(AuthContext)
    return (
        <NavigationContainer>
            <Roots.Navigator
                initialRouteName={'Welcome'}
            >
                <Roots.Screen
                    name={'Home'}
                    component={BottomTabNavigation}
                    options={({ navigation }: any) => ({
                        headerLeft: () => (null),
                        headerRight: () => (
                            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                                <ProfileIcon />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Welcome'}
                    component={Welcome}
                />
                <Roots.Screen
                    name={'Search'}
                    component={Search}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Login'}
                    component={Auth}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Signup'}
                    component={Auth}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Post'}
                    component={Post}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Content'}
                    component={ContentPage}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'REPORT'}
                    component={Report}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Profile'}
                    component={Profile}
                    options={({ navigation }) => ({
                        headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        ),
                        headerRight: () => (
                            <TouchableOpacity onPress={() => { signOut(); navigation.navigate('Welcome')}}>
                                <SignOutIcon />
                            </TouchableOpacity>
                        )
                    })}
                />
                <Roots.Screen
                    name={'Notification'}
                    component={Notification}
                    options={({navigation}) => ({
                          headerLeft: () => (
                            <TouchableOpacity
                                onPress={() => navigation.goBack()}
                            >
                                <HeaderBack />
                            </TouchableOpacity>
                        ),
                    })}
                />

            </Roots.Navigator>
        </NavigationContainer >
    );
}

