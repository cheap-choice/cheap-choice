import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { BottomButton } from './Bottom-button';

export const BottomTab: React.FC<any> = ({ navigation }) => {
  const insets = useSafeAreaInsets();
  
  return (
    <View style={[{ width: '100%', height: Math.max(insets.bottom + 64), backgroundColor:'white' }]}>
      <View style={[styles.box]} >
        <BottomButton type={'Search'} onClick={() => navigation.push('Search')} />
        <BottomButton type={'Post'} onClick={() => navigation.push('Post')} />
        <BottomButton type={'Notification'} onClick={() => navigation.push('Notification')} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create(({
  box: {
    width: '100%',
    backgroundColor: 'white',
    height: 64,
    paddingHorizontal: 30,
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
}))