import React from 'react';
import { StyleSheet, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { BellIcon, PostButtonIcon, SearchIcon } from '../icons/index';

type buttons = {
  onClick: Function,
  type: 'Post' | 'Search' | 'Notification'
}

export const BottomButton: React.FC<buttons> = ({ onClick, type }) => {
  return (
    <>
      <TouchableOpacity onPress={() => onClick && onClick()}>
        {type == 'Post' &&
          <View style={styles.scale}>
            <View style={styles.button}>
              <PostButtonIcon />
            </View>
          </View>
        }
        {type == 'Search' &&
          < SearchIcon />
        }
        {type == 'Notification' &&
          < BellIcon />
        }
      </TouchableOpacity>
    </>
  )
}

const styles = StyleSheet.create({
  scale: {
    backgroundColor: '#FAFAFA',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 55,
    height: 64,
    width: 64,
  },
  button: {
    backgroundColor: '#FFFFFF',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    height: 54,
    width: 54,
    borderStyle: 'solid',
    borderWidth: 1,
    shadowOffset: { height: 10, width: 10 },
    shadowColor: 'rgba(0, 0, 0, 0.4)',
  }
})